/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open('pwa').then(cache => {
            return cache.addAll([
                'service-worker.js',
                'index.html',
                'script.js',
                'style.css'
            ]).then(() => self.skipWaiting());
        })
    )
});

/* Serve cached content when offline */
self.addEventListener('fetch', function (e) {
    e.respondWith(
        caches.match(e.request).then(function (response) {
            return response || fetch(e.request);
        })
    );
});