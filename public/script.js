if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service-worker.js')
        .then(function(registration) {
            console.log('Registration successful, scope is:', registration.scope);
        })
        .catch(function(error) {
            console.log('Service worker registration failed, error:', error);
        });
}

window.setInterval(function () {
    const x = parseFloat(document.getElementById('x').value);
    const y = parseFloat(document.getElementById('y').value);
    const margeInterne = parseFloat(document.getElementById('margeInterne').value);
    const margeExterne = parseFloat(document.getElementById('margeExterne').value);
    const biseau = parseFloat(document.getElementById('biseau').value);
    const talon = document.getElementById('talon').checked;

    const xTotal = x + (2 * margeInterne) + (2 * biseau) + (2 * margeExterne);
    const yTotal = y + (2 * margeInterne) + (talon ? 0.5 : 0) + (2 * biseau) + (2 * margeExterne);

    document.getElementById('xTotal').innerText = xTotal;
    document.getElementById('yTotal').innerText = yTotal;
}, 200);
